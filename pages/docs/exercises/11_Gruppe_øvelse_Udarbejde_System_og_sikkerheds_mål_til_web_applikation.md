# Øvelse 11 - Udarbejd system og sikkerhedsmål til web applikation

## Information
I denne øvelse, skal i udarbejde system mål og sikkerheds mål til en web applikation.

Applikationen i skal udarbejde system og sikkerheds mål til kan findes her: [.Net reference webshop](https://github.com/dotnet-architecture/eShopOnWeb).
Brug værktøjet _git_ til at klone reposistoriet med, og start applikationen ved at følge instruktionerne i readme afsnittet _Running the sample locally_.

I øvelsen skal der udarbejdes system mål repræsenteret i brugstilfælde format, og herefter skal der afledes
sikkerheds mål ud fra hver system mål.
   
****

**Husk at holde det korrekte abstraktions niaveu i brugstilfældene, altså de skal vise hvordan systemet opfylder et forretningsmæssigt formål**  
  
**Husk at anvende CIA når sikkerhedsmål udledes fra system mål**

## Instruktioner  
1. Identificer titlerne til 3 brugstilfælde, og noter dem på et brugstilfælde diagram.  
_Bemærk der står identificer, ikke udarbejd, her er det nok blot at havde brugstilfælden noteret på et brugstilfælde diagram_ .
2. Ud fra brugstilfælde diagrammet skal i nu brainstorme og identificer misbrugstilfælde, identificer som minimum 3 misbrugstilfælde, og noter dem på brugstilfælde diagrammet.  
_Bemærk at der stadig står identificer, ikke udarbejd_  
3. Udarbejd nu som minimum 2 brugstilfælde.  
_At udarbejd et brugstilfæde betyder at brugstilfældet nu skal beskrives_  
4. Udarbejd nu som minimum 1 misbrugstilfælde for hvert udarbejdet brugstilfælde.  



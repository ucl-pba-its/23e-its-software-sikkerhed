# Øvelse 13 - Udarbejd trusselsmodellering til web applikation

## Information
I denne øvelse skal der udarbejdes en trusselsmodellering til alle de misbrugstilfælde der blev
udarbejdet i forrige opgave.

Modellering generelt er ikke detalje orienteret. En model indeholder kun de nødvendige detaljer og ikke mere.
Det samme gøre sig gældende for trusselsmodellering. Lav F.eks. et blok diagram og påtegn kun de store dele af 
systemet, og begynd at brainstorme omkring evt. trusseler ud fra STRIDE. Senere kan man altid gå yderlige i 
dyben, men det er vigtig at man ikke går alt for detalje orienteret tilværks, da man  risikerer at modelleringen
bliver uoverskuelig. Det er vigtigt og huske, at trusselsmodellering handler om at identificerer evt. sårbarheder
med også om at løbenden opnå bedre overblik over systemet, samt viden. Det er altså en iterativ process, og bliver
ikke perfekt den første gang.

STRIDE hjælper indledningsvist med at identificer hvilken potentielle sårbareder der kan opstå mellem de
forskellige dele af arkitekturen. Men for teams hvor arbejdet med sikkerhed er nyt, vil man ofte skulle ud og
søge denne viden. Her kan F.eks. Google som udgangspunkt hjælpe, F.eks. ved en søgning på _"Information disclosure  vulnerabilities"_.
En simple test kan også hjælpe med at afklare om der er sårbarheder. Er der F.eks. noget bruger input som overskrider en tillidsgrænse
kan man afprøve det ved forsøge sig med forskellige kendte farlige inputs, F.eks. `<script>alert('You got hacked')</script>` eller `Robert') DROP TABLE Students;--?`.

**HUSK, det er vigtig at man ikke hænger sig for meget i detaljerne, eller bliver trusselsmodelleringen en uendelig stor opgave! Det er en iterativ proces**

## Instruktioner  
1. Identificer arkitekturen i web applikationen.
2. Lav et diagram med arkitekturen fra web applikationen (Gerne blok diagram, som i dagens forberedelse).
3. Påtegn tillidsgrænser på diagrammet.
4. For hvert misbrugstilfælde(dog en adgangen), brug STRIDE til at identificer sårbarheder.
_Start med det misbrugstilfælde med den højeste risiko_
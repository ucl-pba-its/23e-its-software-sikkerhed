# Øvelse 15 - Forstå koden

## Information
Formålet med denne øvelse er opnå en forståelse for samehængen mellem
implementeret kode og det domæne koden arbejder indenfor.

Repositoriet du skal anvende i denne øvelse kan findes her:
[https://github.com/mesn1985/missingDomain](https://github.com/mesn1985/missingDomain)


## Instruktioner  
1. klone repositoriet.
2. Gennemgå koden og forsøg at forstå koden.
3. Vurder om koden er sikker.
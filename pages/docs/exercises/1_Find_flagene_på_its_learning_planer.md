# Øvelse 1 - Find flagene på its learning planer.

## Information
Formålet med denne øvelse er at danne et overblik over its learning planer, og hvilken 
information der kan findes på its learning.


## Instruktioner

Der skal findes 3 flag på its learning planer. Hvert trin i instruktionerne, er en ledetråd til
hvor flaget kan findes. Lokationen til alle 3 flag skal findes. Herudover skal alle flagene være 
i deres oprindelig format (og fremgangsmåden til at konventer dem til deres oprindelig format skal kunne beskrives).
Altså, opgaven er som følger:

- Find lokationen til de 3 flag.
- Konvetere flagene til deres oprindelig format
- Vær forberedt på at kunne beskrive fremgangsmåden for at konventer hvert flag til deres oprindelige format.   

### Ledetråd til hvert  af de 3 flag
- Forberedelsen til den lektion hvor et af emnerne er statisk kode analyse.
<!-- 719d067b229178f03bcfa1da4ac4dede - software - md5 -->
- En resource til den sidste lektion, inden arbejdet med eksamens projektet starter.
<!-- PHNjcmlwdD5BbGVydCgiZXIiKTwvc2NyaXB0Pg%3D%3D - <script>Alert("er")</script> (URL encoded and base64 encoded) -->
- En refleksion efter du har forberedt dig til undervisning i input validering.
<!-- Jmx0O2RpdiZndDsgZm9yYW5kcmVsaWd0ICZsdDsvZGl2Jmd0Ow== - <div> forandreligt </div> (HTML encoded and base64 encoded -->
  

## Links
[Base64 encoding & decoding](https://www.base64decode.org/)  
[HTML decoding](https://emn178.github.io/online-tools/html_decode.html)  
[URL Decoding](https://www.urldecoder.org/)  
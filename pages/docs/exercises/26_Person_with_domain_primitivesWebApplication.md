# Øvelse 26 - Fra DTO til model med domæne primitiver.

## Information

Når ASP .Net core frameworket modtager data fra en anden applikation, håndteres denne data 
først i en Controller klasse (Service laget). Dataen kan enten være en simple type såsom int eller string,
men den kan også være en kompleks type sådan som du så i [Opgave 20](./20_Input_validering_og_model_binding.md).
Komplekse typer der bliver brugt til at modtage data omtales ofte som et _Data transfer object_(Herefter DTO),
Eksempelet fra opgave 20 er vist herunder. 

```Csharp
 public class PersonDTO
    {
        [NotNull]
        public string Firstname { get; set; }
        [NotNull]
        public string Lastname { get; set; }
    }
```
Som navnet hentyder, er dette objekt beregnet til at sende data til andre applikationer, eller modtage data
fra andre applikation. Selvom objektet har attributer og kan input valideres, er der intet til hindre for at 
objektet kan oprettes et andet sted i softwaren, uden at overholde nogen domæne regler. Et objekt af typen `PersonDTO`
kan oprettes et hvilket som helst sted i softwaren med et constructor kald `PersonDTO personDTO = new PersonDTO;`.
Dette er uhensigtsmæssigt da en udvikler kan komme til at anvende dette objekt andre steder i koden, uden at der
er nogen garanti for at data i objektet overholder gældende domæne regler. Derfor bør DTO typer kun eksistere
og anvendes i servicelaget(Controller klasser i ASP .Net core). Øvrige dele af softwaren bør kun anvende objekter
der som minimum håndhæver invarians, men også gerne domæne primitiver.

i [Opgave 20](./20_Input_validering_og_model_binding.md) blev det modtaget DTO objekt konventeret til et Person objekt 
der håndhæver invarians, som vist nedenfor i udsnittet fra `NameController` klassens `ValidateNme` metode.  
```Csharp
     if (!ModelState.IsValid)
            {
                return View("Index", new {nameIsValid = false, showNameEvaluation = true });
            }

            //DI intentionally omittede here for clarity
            PersonRepository personRepository = new PersonRepository();

            Person personWithInvariance = new Person(person.Firstname, person.Lastname);

            personRepository.AddPerson(personWithInvariance);
```
Dette er god praksis. Men Person objektet bliver ikke oprettede med domæne primitiver. Dette skal gøres i 
denne opgave. Projektet der skal arbejdes med er stadig _InvarianceDTOandModel_ fra repoet [https://github.com/mesn1985/InputValidationsBasicExercises](https://github.com/mesn1985/InputValidationsBasicExercises)


## Instruktioner
1. Opret en Firstname domæne primitive i folderen _Models_ (Regler for navnet er det samme som tidligere).
2. Opret en Lastname domæne primitive i folderen _Models_ (Regler for navnet er det samme som tidligere).
3. Udskift de to private fields i Person, med domæne primitiverne.
4. Lad Constructoren tage de to domæne primitiver som argumenter, og brug det til at sætte de to fields.
5. I klassen _NameControllers_ _ValidateName_ metode. Sikker dig at Person bliver initialseret korret.
6. Test det ved at gøre applikationen et par gange med noget valid input.
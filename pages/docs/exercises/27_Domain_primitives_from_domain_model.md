# Øvelse 27 - Opsamlende øvelse med domæne primitiv

## Information
Formålet med denne opgave er at samle op på den viden om domæne primitiver du har
opnået i de to forrige øvelser.  
  
Du skal selvstændigt implementer følgende model.  
![Car Domain model](./Images/CarDomainModel.jpg)

Husk hvert domæne primitive skal (indenfor realismens grænser) håndhæve
invarians således at den overholder reglerne fra virkelighedens domæne.

## Instruktioner
1. Implementer domæne primitiverne
2. Implementer Bil klassen med domæne primitiver.
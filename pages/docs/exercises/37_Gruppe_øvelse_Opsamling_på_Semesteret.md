# Øvelse 37 - Gruppens opsamling på semesteret.

## Information
Formålet med denne øvelse er vidensdeling og overblik internt i gruppen ift. til de emner,
der er blevet gennemgået i semesteret indtil videre.

Erfaringer fra tidligere semestere har vist at dette kan være en god ide, da gruppen snart skal lave eksamens projekt sammen.

Gruppen skal sammen at forsøge og skabe overblik over de emner der er blevet gennemgået indtil videre.
Her kan der med fordel tages udgangspunkt i fagets plan på _It's learning_. Herfra kan de overordnet emner noteres
ned (På F.eks. et Whiteboard), herefter kan underemner,øvelser o.l. noteres ned.

Herudover bør gruppen forsøge at skabe overblik over hvilken læringsmål de har været i berøring med i løbet
af undervisningen, og forsøge at knytte læringsmålene til konkrette emner.

Gruppen bør forsøge at skabe overblik over styrker og svagheder. F.eks. Hvem er god til software udvikling og 
har styr på at læse kode?, hvem er skarp til at skrive rapport?

Herudover kan der også dannes et overblik over læringsmålene fra faget _Software sikkerhed_ og faget _Webapplikation sikkerhed_.
Emner fra fra Webapplikation sikkerhed må gerne inddrages i software sikkerheds projektet. Emnerne fra faget software sikkerhed 
vægtes dog stadig højst til eksaminationen. 

Dette er dog kun forslag, den process der understøtter gruppen bedst kan anvendes. Det vigtige er at der bliver
skabt overblik i gruppen.

Til eksamen skal gruppen analyser en usikker applikation (kildekoden bliver udleveret) og komme med forslag til 
forbedringer af anvendt praksis i koden og samt hvilket værktøjer der løbende kan anvendes. Herudover vil der også
skulle laves forslag til hvordan fokus på sikkerhed kan indgå i udviklingsprocessen. **Dette kommer i en officiel eksamens projekt beskrivelse i de kommende uger**

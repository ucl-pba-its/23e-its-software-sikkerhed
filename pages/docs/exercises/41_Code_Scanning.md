# Øvelse 41 - Code skanning i repositoriet.

## Information
Formålet med denne øvelse er at introducerer konceptet sikkerheds skanning af kode, omtalt som `Security code scanner`.
Sikkerheds skanning er ligesom _taint analyse_, en statisk kode analyse.

Github benytter sig at [codeql](https://codeql.github.com/) til sikkerheds skanninger. Hvis et repository er offenligt 
tilgængeligt (public), så er anvendelsen af sikkerheds kode skanninger gratis. I øvelse skal sikkerheds kode skanneren 
anvendes på repositoriet.


## Instruktioner
1. Sæt repoet visibility til `public`. vejledning kan findes [her](https://docs.github.com/en/repositories/managing-your-repositorys-settings-and-features/managing-repository-settings/setting-repository-visibility)
2. Tænd for `Security code scan`. vejledning findes [her](https://docs.github.com/en/code-security/code-scanning/enabling-code-scanning/configuring-default-setup-for-code-scanning-at-scale).
3. Afvent at `Security code scan` er færdig med opsætning, og har udført skanningen.
4. I repoet, gå til fanen security, og se resultaterne af kode skanningen. Hvilken sårbarheder blev der fundet? og ligner det de sårbarheder Taint analysen fandt? hvad er forskellen?
# Opgave 4 (Gruppe øvelse) - Fagets læringsmål. 

## Information
Formålet med denne øvelse, er at alle studerende får en overordnet forståelse for indholdet i faget _Software sikkerhed_,
men også begynder at opnå en forståelse for hvordan der skal arbejdes med læringsmålene i faget _Software sikkerhed_ såvel
som øvrige fag på uddannelsen.  
  


## Instruktioner
**Noter alle besvarelser**  
  
1. For hvert læringsmål skal gruppen diskuter og noter hvad læringsmålet betyder.
2. Gruppen skal sammenligne læringsmålene fra _software sikkerhed_ med læringsmålene i _Web applikation sikkerhed_, og noter alle evt. overlap mellem fagene. 

**Læringsmålene kan i finde i studieordningen og valgfags kataloget, eller its learning planer**

## Links

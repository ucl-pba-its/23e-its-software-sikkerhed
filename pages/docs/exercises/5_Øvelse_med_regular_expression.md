# Opgave 5 (Individuel øvelse) - Regular expression  

## Information
Formålet med denne øvelse, er at øge bevidstheden om tilgangen til opgave løsning, når
man står overfor en opgave der som udgangspunkt kan virke uoverskuelig, eller handler om et
emne som man endnu ikke er introduceret til.

**Alle hjælpe midler er tilladt**

## Instruktioner
I denne opgave skal i udarbejde et regular expression (Regex) som kan detekter mønsteret
for et dansk cpr nummer. Herefter skal i producerer et eksempel i C# som bruger Regex mønster, til
at verificere CPR numre.

_Når i har fundet et mønster, kan i teste det med [Regex 101](https://regex101.com/)_


## Links
[Regex 101](https://regex101.com/)
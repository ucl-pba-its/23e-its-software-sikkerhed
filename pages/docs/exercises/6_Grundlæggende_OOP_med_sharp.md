# Øvelse 6 - Grundlæggende OOP med C sharp

## Information
Formålet med øvelsen er at sikre en grundlæggende forståelse for C# og de objekt orienteret programmering med denne.
Øvelsen tager udgangspunkt i en C# tutorial fra Microsoft, og en del af øvelsen er brugen af dokumentation.
  
Instruktionerne fungerer som en læse vejledning til tutorial, og fremhæver hvad du bør være særligt
opmærksom i hvert af afsnittene efter du har implementeret jvf. tutorial. Efter implementering af hvert afsnit, bør du læse 
vejledningen for hvert afsnit.

link til tutorial:  
[Tutorial](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/tutorials/classes)

## Pratiske læringsmål med øvelserne
-   Den studerende oprette en klasse og forstår dennes anvendelse.
-   Den studerende anvende Constructor til initialisering af objekter.
-   Den studerende kan anvende Constructor til initialisering af objekter.
-   Den studerende har grundlæggende forståelse for Indkapsulering i objekter ved brug af private accessors.

## Instruktioner

### Prerequisites
I dette afsnit benævnes de grundlæggende værktøjer som kan anvendes til implementering
af software med C#. Visual studio er microsoft egen IDE(Integrated development enviroment), og
tilbyder en del støtte funktioner til implementering af software med C#. Visual studio code er
en text editor som også er udviklet af Microsoft. Denne kan udvides med en plugins, således at  
denne også kan fungere som IDE.  
SDK som nævnes er _Software Development kit_, og indeholder værktøjer som kræves for at udvikle
software med C#, F.eks. compiler (Roslyn), CLI værktøj (Dotnet) osv.  

### Create your application
I dette afsnit handler om oprettelse af projektet, som en konsol applikation.
I tutorialen anvendes `Dotnet new console` til at oprette projektet. Dette kan
også gøres i visual studio, som vist [her](https://learn.microsoft.com/en-us/visualstudio/get-started/csharp/tutorial-console?view=vs-2022)  
  
**Husk at projektet skal oprettes i en folder der hedder classes**  
  
### Define the bank acccount type
I dette afsnit skal du oprette filen _BankAccount.cs_ i projektet, og i filen
implementerer koden til klassen Bank Account. Overvej følgende punkter, efter implementeringen:

- Hvad er _Number_,_Owner_ og _balance_ ?
- Hvad er _MakeDeposit_ og _MakeWithdrawal_ ?
- Hvorfor omtales klasser også som typer?

### Open a new account
I dette afsnit implementeres en construtor der tager imod 2 pararmetere. Herudover
implementeres en privat statisk variable. Overvej følgende punkter, efter implementeringen:
  
- Hvorfor implementer en constructor som tager imod 2 pararmetere?
- Kan objektet stadig initialiseres uden pararmetere?
- Hvad betyder det at _s_accountNumberSeed_ er privat? (Kan variablen bruges uden for klassen, i F.eks. konsol applikationen?)

### Create deposits and withdrawals
I dette afsnit implementeres en ny type(klasse) _Transaction_. som herefter anvendes 
til udførelse deponering og udtræk. Overvej følgende  punkter, efter  implementeringen:
  
- Hvad er fordelen ved at transaktioner har sin egen type(klasse)?
- hvorfor er listen _allTransactions_ private? (indkapsulering)
- Hvad sker der hvis koden ` throw new ArgumentOutOfRangeException(nameof(amount), "Amount of deposit must be positive");` eksekveres?
- Hvorfor skal metoden _MakeDeposit_ kaldes i _Constructor_?

### Challenge - log all transactions
I dette afsnit implementeres en metode som kan generer en tekst streng, som viser 
hele transaktions historikken. Overvej følgende punkter:  
- Hvordan anvendes den private liste _alltransactions_ , og hvorfor kan denne tilgang være en god ide?

## Links

# Øvelse 7 - (Gruppe øvelse) Fælles forståelse for system & sikkerhedsmål

## Information
Formålet med denne øvelse er at gruppen laver en fælles afstemmeles af forståelsen for
hvad system mål og sikkerheds mål er, samt tankegangen bag _Secure by design_. Dette træner brugen af 
termanonlogier samt fagbegreber. Dette er vigtig når man skal kommuniker internt i gruppen,
og i proffesionel samehængen(F.eks. på en arbejdsplads). Endvidere understøtter vidensdelingen
og forståelse for fag termerne samt begreber den  videre læring og eksaminationen. 


## Instruktioner  
Gruppen skal diskuterer dagens forberedelse, som er:  
- Introduktion til sikker software udvikling - del 1.  
- Introduktion til sikker software udvikling - del 2.  
- Introduktion til sikker software udvikling - del 3.  
- _Secure by design_ kapitel 1. 

1. Hvad er læren fra Öst-Götha Bank røveriet?
2. Bør sikkerhed tænkes ind i software udvikling som en selvstændig Feature?
3. Hvorfor er det relevant at kende system målene når sikkerhed skal tænkes ind?
4. Hvilket abstraktions niveau(detaljegrad) bør system mål havde?
5. Hvorfor er det en fordel at ulede sikkerheds mål fra system mål?

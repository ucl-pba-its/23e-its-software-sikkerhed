#Nyttige links   
Her på siden  kan du finde nyttige links ift. programmering og software udvikling.

# C# og .Net Tutorials  
  
## Opsætning af udvikling miljø
[Opsætning af udviklings miljø](https://learn.microsoft.com/en-us/dotnet/csharp/tour-of-csharp/tutorials/local-environment)  
  
## Linkedin learning grundlæggende kurser.  
[Grundlæggende programmering i c#](https://www.linkedin.com/learning/learning-c-sharp-8581491/what-you-should-know?u=57075649)  
[Grundlæggende programmering i C# (control flow)](https://www.linkedin.com/learning/c-sharp-essential-training-1-types-and-control-flow/what-you-should-know)  
[Introduktion til web api'er med c# og .Net](https://www.linkedin.com/learning/building-web-apis-with-asp-dot-net-core-in-dot-net/what-you-should-know-19430362?resume=false&u=57075649)  
  
## Microsoft docs grundlæggende tutorials  
[Objekt orienteret programmering i C#](
https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/tutorials/oop)  
[Klasser i C#](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/tutorials/classes)  
[Avanceret sprog koncepter i C#](https://learn.microsoft.com/en-us/shows/c-advanced/)  
[Grundlæggende C#](https://learn.microsoft.com/da-dk/shows/csharp-101/?wt.mc_id=educationalcsharp-c9-scottha )  
[Grundlæggende MVC web application i .Net](https://learn.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/start-mvc?view=aspnetcore-7.0&tabs=visual-studio)  
  
## Tim corey   
[Tim coreys youtube videoer](https://www.youtube.com/@IAmTimCorey)  
  
##Eksempler på applikationer.  
  
[https://github.com/dotnet-architecture](https://github.com/dotnet-architecture)   
Microsoft applikation eksempler lavet med C#/.Net

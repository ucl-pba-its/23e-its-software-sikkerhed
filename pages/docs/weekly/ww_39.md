---
Week: 39
Content: Udviklingsproces samt domæne modellering
Material: Se ugentligt plan
Initials: MESN
---

# Uge 39 - Udviklingsproces samt domæne modellering

## Mål for ugen

- Den studerende har forståelse for software sikkerhedens 3 søjler.
- Den studerende har forståelse for hvert af software sikkerhedens 7 berøringspunkter. 
- Den studerende har forståelse for samehængen mellem software udvikling og domæne forståelse.
- Den studerende har grundlæggende forståelse for modellering af et domæne.

### Praktiske mål
- Hver studerende har udarbejdet en simpel domæne model.
- Hver studerende har udarbejdet et simpel overblik over software sikkerhedens 7 berøringspunkter.

### Læringsmål
#### Viden
- Den studerende har viden om hvilken betydning programkvalitet har for it-sikkerhed.
- Den studerende har viden og forståelse for security design principles
#### Færdigheder
- Den studerende kan definere lovlige og ikke-lovlige input data.
### Kompetencer
- Den studerende kan Håndterer risikovurdering af programkode for sårbarheder.

### Mandag
|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Opsamling fra sidst |
| 8:45  | Oplæg: udviklingsproces og sikkerhed |
| 9:00  | Gruppe øvelse: software sikkerhedens 3 søjler og udviklingsprocessen |
| 9:35  | Opsamling |
| 9:45  | Pause  |
|10:00  | Opsamling |
|10:10  | Individuel øvelse: Forstå koden |
|10:30  | Opsamling |
|10:35  | Oplæg om domæne modellering |
|10:45  | Gruppe Øvelse: Domæne modellering |
|11:20  | Opsamling |
|11:30  | Lektioner slut |

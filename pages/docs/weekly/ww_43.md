---
Week: 43
Content: Fejlhåndtering i software og unit test
Material: Se ugentligt plan
Initials: MESN
---

# Uge 43 - Fejlhåndtering i software og unit test.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål
- Den studerende har implementeret global exception håndtering i ASP .Net Core.
- Den studerende har implementeret en unit test til en controller metode.
- Den studerende har implementeret unit test til en domæne primitiv.

### Læringsmål
  
#### Viden
- Den studerende har viden om trusler mod software.
- Den studerende har viden om fejlhåndtering i software.
#### Færdigheder
- Den studerende kan tage højde for sikkerhedsaspekter ved håndtering af forventede og uventede fejl.
- den studerende kan definere valid og ikke valid data til test

### Mandag
|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Oplæg: Exceptions |
| 8:45  | Individuelle øvelser: Håndtering af exceptions |
| 9:45  | Pause |
| 10:00  | Opsamling på øvelsen med exceptions |
| 10:10  | Oplæg: Unit test |
| 10:25  | Individuelle øvelser: Unit test |
| 11:20  | Opsamling Øvelse med unit test |
| 11:30  | Dagens lektion slut |

